package appolis.kurt.panningimage;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnGenericMotionListener;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

public class MainActivity extends Activity {
	int x = 0;
	HorizontalScrollView activity_main_hsv;
	ImageView activity_main_iv;
	FrameLayout activity_main_rl;
	boolean test = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setProgressiveScroll();
	}

	private void setProgressiveScroll() {
		activity_main_hsv = (HorizontalScrollView) findViewById(R.id.activity_main_hsv);
		activity_main_iv = (ImageView) findViewById(R.id.activity_main_iv);
		activity_main_rl = (FrameLayout) findViewById(R.id.activity_main_rl);

		activity_main_hsv
				.setOnGenericMotionListener(new OnGenericMotionListener() {

					@Override
					public boolean onGenericMotion(View v, MotionEvent event) {
						Log.d("details event:: ", event.toString());
						return false;
					}

				});
		doScroll();
	}

	public void doScroll() {
		new Thread() {

			@Override
			public void run() {
				for (int i = 0; i <= 720; i++) {
					activity_main_rl.post(new Runnable() {

						@Override
						public void run() {
							activity_main_hsv.scrollTo(x, 0);
							x++;
							Log.d("scrollView: ", "x: " + x);
						}
					});
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}.start();
	}
}